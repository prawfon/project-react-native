import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { InputItem, WhiteSpace, Button } from '@ant-design/react-native';
import axios from 'axios';

class Register extends Component {
    state = {
        visible: false,
        email: "",
        password: "",
        firstName: "",
        lastName: "",

    }
    goToLogin = () => {
        this.props.history.push('/Login')
    }
    goToMovie = () => {
        this.props.history.push('/Movie', { user: this.state.email, password: this.state.password })
    }

    // goToRegister = () => {
    //     this.props.history.push('/Register')
    //     // this.props.history.push('/Register', { user: this.state.email, password: this.state.password })
    // }
    goToRegister = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
            method: 'post',
            data: {
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName,

            }
        }).then(res => {
            const { data } = res
            const { user } = data
            this.props.history.push('/Login')
        }).catch(e => {
            console.log("error ", e.response)
        })
    }


    render() {
        return (
            <ImageBackground source={require('./min.png')} style={styles.background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={[styles.layout1, styles.centerLayout1]}>
                            <View style={styles.center}>
                                <Image source={require('./movie2.png')} style={styles.profile} />
                                <Text style={styles.textLogo}>REGISTER</Text>
                            </View>

                        </View>
                        <View style={[styles.layout2, styles.centerLayout2]}>
                            <InputItem
                                type="email"
                                value={this.state.email}
                                onChange={email => { this.setState({ email, }); }}
                                placeholder="E-mail"
                            >
                            </InputItem>
                            <InputItem
                                type="number"
                                value={this.state.password}
                                onChange={password => { this.setState({ password, }); }}
                                placeholder="Password"
                            >
                            </InputItem>
                            <InputItem
                                type="text"
                                clear
                                value={this.state.firstName}
                                onChange={firstName => { this.setState({ firstName, }); }}
                                placeholder="First Name"
                            >
                            </InputItem>
                            <InputItem
                                type="text"
                                value={this.state.lastName}
                                onChange={lastName => { this.setState({ lastName, }); }}
                                placeholder="Last Name"
                            >
                            </InputItem>

                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    {/* <TouchableOpacity onPress={this.goToHome}>
                                <WhiteSpace />
                                    <Text style={styles.textButton}>Login</Text>
                                </TouchableOpacity> */}
                                    {/* <Button onPress={() => this.goToRegister()} >Register</Button> */}
                                    <Button  activeStyle={{ backgroundColor: 'white' }} onPress={() => this.goToRegister()}>REGISTER</Button>
                                 
                                </View>
                            </View>


                            {/* <View style={styles.loginFooter}> */}
                            {/* <View>
                                <TouchableOpacity onPress={this.goToRegister}>
                                    <Text style={{fontSize: 17, color: 'black'}}>Register</Text>
                                </TouchableOpacity>
                            </View> */}

                            {/* </View> */}

                        </View>
                    </View>
                </View >

            </ImageBackground >

        );
    }
}

const styles = StyleSheet.create({

    background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 0.5,
        flexDirection: 'column'
    },

    layout2: {

        flex: 1,
        flexDirection: 'column'
    },
    textLogo: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginmiddle: {
        // flex: 0.5,
        flexDirection: 'row'
    },

    loginFooter: {
        flex: 0.5,
        flexDirection: 'row'
    },

    profile: {
        borderRadius: 10,
        width: 150,
        height: 150
    },

    usernameIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    passwordIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    loginButton: {
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 300,
    },

    textStyle: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15
    },

    textButton: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },

    centerLayout1: {
        top: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        top: 20,
        alignItems: 'center',
        
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        flex: 1,
        margin: 15
    }

})


export default Register
