import React, { Component } from 'react';
import { Route,Switch, Redirect } from 'react-router-native';
import { Provider } from 'react-redux';
import { store, history } from './Reducers/AppStore'
import { ConnectedRouter } from 'connected-react-router'
import Login from './Login';
import Register from './Register';
import Movie from './Movie';
import Profile from './Profile';
import detailBooking from './detailBooking';
import EditProfile from './EditProfile';
import Booking from './Booking';
import History from './History';

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exaat path="/Login" component={Login} />
                        <Route exact path="/Register" component={Register} />
                        <Route exact path="/Movie" component={Movie} />
                        <Route exact path="/detailBooking" component={detailBooking} />
                        <Route exact path="/Profile" component={Profile} />
                        <Route exact path="/EditProfile" component={EditProfile} />
                        <Route exact path="/Booking" component={Booking} />
                        <Route exact path="/History" component={History} />
                        <Redirect to="/Login" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        );
    }
}

export default Router