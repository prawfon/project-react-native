export default (state = {}, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return action.user
        case 'EDIT_USER':
            return {
                ...state,
                user: {
                    ...state.user,
                    firstName: action.firstName,
                    lastName: action.lastName,
                }

            }
        default:
            return state
    }
}
