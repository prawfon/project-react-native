import React, { Component } from 'react';
import {  StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { WhiteSpace, Button, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios';
class Profile extends Component {
    state = {
        visible: false,
        email: "",
        password: "",
        firstName: "",
        lastName: "",
        imagePath: ''

    }
    onClickBack = () => {
        this.props.history.push('/Movie')
    }

    goToLogin = () => {
        this.props.history.push('/Login')
    }
    goToEditProfile = () => {
        this.props.history.push('/EditProfile')
    }
    
    selectImage = () => {
        console.log("55", ImagePicker)
        console.log("pros token", this.props.user.user.token)
        // console.log("55",ImagePicker.showImagePicker)
        // console.log("55",ImagePicker.showImagePicker())
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${this.props.user.user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log("errror rrrr", error.response)
                    })
            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }


    render() {
        const { user } = this.props
        console.log('ppppp: ', user)
        return (
            <ImageBackground source={require('./profileMinions.png')} style={styles.background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={[styles.boxHeader]}>
                            <View style={styles.headerBox1}>
                                <TouchableOpacity onPress={this.onClickBack}>
                                    <View style={[styles.rowIcon]}>
                                        <Icon name="left" size="md" color="white" />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={[styles.headerBox2]}>
                                <Text style={[styles.textprofile]}>Profile</Text>
                            </View>

                        </View>
                      
                        <View style={[styles.layout2, styles.centerLayout2]}>
                            {/* <View style={styles.center}> */}
                            <TouchableOpacity onPress={() => this.selectImage()}>
                                <WhiteSpace />
                                <Image source={{ uri: this.state.imagePath }} style={styles.profile} />
                            </TouchableOpacity>
                            {/* </View> */}
                            <WhiteSpace />
                            <WhiteSpace />
                            <Text style={styles.text}>E-mail: {user.user.email}</Text>
                            <WhiteSpace/>
                            <Text style={styles.text}>First name: {user.user.firstName}</Text>
                            <WhiteSpace/>
                            <Text style={styles.text}>Last name: {user.user.lastName}</Text> 
                            {/* <Text style={styles.text}>E-mail: </Text>
                            <Text style={styles.text}>First name: </Text>
                            <Text style={styles.text}>Last name:</Text> */}
                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    <Button onPress={this.goToEditProfile}>EDIT</Button>

                                </View>

                                <View style={styles.loginButton}>

                                    <Button type="primary" onPress={this.goToLogin}>LOGOUT</Button>
                                </View>
                            </View>

                        </View>
                        {/* <View style={styles.box}>
                            <Button
                                title="Select Image"
                                onPress={() => this.selectImage()}
                            >Select Image</Button>
                        </View> */}

                    </View>
                </View >

            </ImageBackground>

        );
    }
}

const styles = StyleSheet.create({

    background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 0.5,
        flexDirection: 'column'
    },

    layout2: {

        flex: 1,
        flexDirection: 'column'
    },
    textprofile: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    
    },
    text: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
        // padding: 30

    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: 'black',
        borderBottomWidth: 3,
        borderBottomColor: 'white',
        justifyContent: 'center',
        padding: 15,
    },
    rowIcon: {
        flex: 0,
    },
    loginmiddle: {
        flex: 0.5,
        flexDirection: 'row'
    },

    loginFooter: {
        flex: 0.5,
        flexDirection: 'row'
    },

    profile: {
        borderRadius: 120,
        width: 150,
        height: 150
    },

    usernameIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    passwordIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    loginButton: {
        backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 100,
        height: 40
    },

    textStyle: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15
    },

    textButton: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        flex: 1,
        margin: 15
    },
    header: {
        backgroundColor: 'black',
        alignItems: 'center',
        flex: 0.14,
        flexDirection: 'row'
    },

    headerBox1: {
        flex: 0.5,
        backgroundColor: 'black'
    },

    headerBox2: {
        flex: 0.8,
        backgroundColor: 'black',
        flexDirection: 'row',
      
    },



})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Profile)
