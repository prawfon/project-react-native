/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Login from './Login';
import Register from './Register';
import Router from './Router';
import Movie from './Movie';
import Profile from './Profile';
import Booking from './Booking';
import {name as appName} from './app.json';


AppRegistry.registerComponent(appName, () => Router);
