import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView,Alert} from 'react-native';
import { Icon, TabBar, Tabs,Grid,Button} from '@ant-design/react-native';
import axios from 'axios'
var options = { weekday: 'short', month: 'short', day: 'numeric' };

// console.log(today.toLocaleDateString("en-US")); // 9/17/2016
// console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016

var tab1 = new Date();
date1 = tab1.getTime()
day1 = tab1.toLocaleDateString("en-US", options)
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
date2 = tab2.getTime()
day2 = tab2.toLocaleDateString("en-US", options)
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
date3 = tab3.getTime()
day3 = tab3.toLocaleDateString("en-US", options)


class listmovies extends Component {

    goToLogin = () => {
        this.props.history.push('/Login')
    }
    goToProfile = () => {
        this.props.history.push('/Profile')
    }
    onClickBack = () => {
        this.props.history.push('/Movie')

    }
    onClickBookingHistory = (item) => {
        this.props.history.push('/Booking', { item: item })

    }


    // onClickProfile = () => {
    //     this.props.history.push('/profile')
    // }
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Showing',
            items: [],
            isLoading: true,
            movies1: [],
            movies2: [],
            movies3: [],
            isLoadingShowing: true,
            tabs: [
                { title: day1 },
                { title: day2 },
                { title: day3 },
            ],
            dates: [
                { title: date1 },
                { title: date2 },
                { title: date3 },
            ]
            // tabs: [
            //     { title: day1, date: tab1 },
            //     { title: day2, date: tab2 },
            //     { title: day3, date: tab3 },
            // ]

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }


    getMovie = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes${this.props.location.state.item._id}`)
            .then(response => {
                this.setState({
                    items: response.data,
                    isLoadingShowing: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate1 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date1
            }
        })
            .then(response => {
                this.setState({
                    movies1: response.data,
                    // isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate2 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date2
            }
        })
            .then(response => {
                this.setState({
                    movies2: response.data,
                    // isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate3 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date3
            }
        })
            .then(response => {
                this.setState({
                    movies3: response.data,
                    // isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.idmovie) {
            Alert.alert('Your number is', this.props.location.state.idmovie + '')
        }

    }
    clickTime = (item) => {
        console.log(item);
        
        this.props.history.push('/cinema', { item: item ,name:this.props.location.state.item})

    }

    componentDidMount() {
        this.getMovie()
        this.getMovieByDate1()
        this.getMovieByDate2()
        this.getMovieByDate3()
    }

    render() {
        const movie = this.props.location.state.item
        // console.log(this.state.movies1);
        console.log(this.state.movies2);
        // console.log(this.state.movies3);


        return (


            <TabBar
                unselectedTintColor="white"
                tintColor="black"
                barTintColor="black"
            >
                <TabBar.Item
                    title="MOVIES"
                    icon={<Icon name="video-camera" />}
                    selected={this.state.selectedTab === 'Movie'}
                    onPress={() => this.onClickBack()}
                >
                    <View style={[styles.contents]}>
                        <View style={[styles.boxHeader]}>

                            <TouchableOpacity
                                onPress={this.onClickBack}
                            >
                                <View style={[styles.rowIcon]}>
                                    <Icon name="left" size="md" color="white" />
                                </View>
                            </TouchableOpacity>

                            <View style={[styles.rowHeader]}>
                                <Text style={[styles.textname]}>{movie.name}</Text>
                            </View>

                        </View>
                        <ScrollView>
                            <View style={[styles.boxContent]}>

                                <View style={[styles.box1]}>

                                    <View style={[styles.rowHeader]}>
                                        <Image source={{ uri: movie.image }}
                                            style={{ width: '100%', height: 250 }} />
                                    </View>


                                    <View style={[styles.rowHeader, styles.center]}>
                                        <Text style={styles.textHead}>
                                            {movie.name}
                                        </Text>
                                        <Text style={styles.textHead}>
                                            <Icon name="clock-circle" size="md" color="#f47373" /> {movie.duration} min
                                        </Text>
                                    </View>
                                </View>
                                <View style={[styles.box1]}>
                                    <Tabs
                                        tabs={this.state.tabs}
                                        tabBarPosition='top'
                                    >
                                        <View>
                                            <Grid
                                                data={this.state.movies1}
                                                hasLine={false}
                                                renderItem={(item) => (
                                                    //console.log(item)
                                                    <View style={{ flex: 1, margin: 3 }}>
                                                        <Text style={{ color: "gray", fontSize: 12 }}>
                                                            <Icon name="sound" size="xxs" color="gray" />{" "}
                                                            {item.soundtrack}
                                                        </Text>
                                                        <Button  onPress={() => this.onClickBookingHistory(item)}>
                                                            <Text style={{ color: 'black'  }}>
                                                                {new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}
                                                            </Text>
                                                        </Button >
                                                    </View>
                                                )}
                                                // itemStyle={{ height: 180, margin: 10 }}
                                            />
                                        </View>
                                        <View>
                                            <Grid
                                                data={this.state.movies2}
                                                hasLine={false}
                                                renderItem={(item) => (
                                                    //console.log(item)
                                                    <View style={{ flex: 1, margin: 3 }}>
                                                        <Text style={{ color: "gray", fontSize: 12 }}>
                                                            <Icon name="sound" size="xxs" color="gray" />{" "}
                                                            {item.soundtrack}
                                                        </Text>
                                                        <Button  onPress={() => this.onClickBookingHistory(item)}>
                                                                <Text style={{ color: 'black'}}>
                                                                {new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}
                                                            </Text>
                                                        </Button >
                                                    </View>
                                                )}
                                                // itemStyle={{ height: 180, margin: 10 }}
                                            />
                                        </View>
                                        <View>
                                            <Grid
                                                data={this.state.movies3}
                                                hasLine={false}
                                                renderItem={(item) => (
                                                    //console.log(item)
                                                    <View style={{ flex: 1, margin: 3 }}>
                                                        <Text style={{ color: "gray", fontSize: 12 }}>
                                                            <Icon name="sound" size="xxs" color="gray" />{" "}
                                                            {item.soundtrack}
                                                        </Text>
                                                        <Button  onPress={() => this.onClickBookingHistory(item)}>
                                                            <Text style={{ color: 'black' }}>
                                                                {new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}
                                                            </Text>
                                                        </Button >
                                                    </View>
                                                )}
                                                // itemStyle={{ height: 180, margin: 10 }}
                                            />
                                        </View>

                                    </Tabs>

                                </View>
                            </View>

                        </ScrollView>
                    </View>
                </TabBar.Item>

{/* 
                <TabBar.Item
                    icon={<Icon name="heart" />}
                    title="MY MOVIES"
                //   selected={this.state.selectedTab === 'Faverite'}
                //   onPress={this.onClickFaverite}
                /> */}

                <TabBar.Item
                    icon={<Icon name="reload-time" />}
                    title="BOOKING"
                  selected={this.state.selectedTab === 'Booking'}
                  onPress={() => this.onClickBookingHistory('Booking')}
                />


                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="PROFILE"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={this.goToProfile}
                />
            </TabBar>

        );
    }
}

export default listmovies

const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imagesize: {
        width: 50,
        height: 50,
        marginLeft: 10

    },
    center: {
        alignItems: 'center',
        //justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373'
    },
    textname: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',

    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: 'black',
        borderBottomWidth: 3,
        borderBottomColor: 'white',
        justifyContent: 'center',
        padding: 15,


    },
    boxContent: {
        flexDirection: 'column',
        backgroundColor: 'white',
        justifyContent: 'center',
        margin: 10

    },
    rowHeader: {
        flex: 1,
    },
    rowIcon: {
        flex: 0,
    },
    box1: {
        flex: 1,
        flexDirection: 'row',
        height: 250
    },
    box2: {
        flex: 1
    }
});