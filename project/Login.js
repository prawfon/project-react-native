import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { InputItem,Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';

class Login extends Component {
    state = {
        visible: false,
        email: "",
        password: "",
        firstName: "",
        lastName: "",
    }
    goToMovie = () => {
        this.props.history.push('/Movie')
    }

    goToRegister = () => {
        this.props.history.push('/Register')
        // this.props.history.push('/Register', { user: this.state.email, password: this.state.password })
    }

    onChangeValue = (index, value) => this.setState({ [index]: value })
    onLogin = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
            method: 'post',
            data: {
                // email: this.state.email,
                // password: this.state.password,
                email: 'prawfon@gmail.com',
                password: '1234'
            }
        }).then(res => {
            const { data } = res
            const { user } = data
            this.props.addUser(data);
            this.props.history.push('/Movie')
        }).catch(e => {
            console.log("error " + e)
            alert('Incorrect Login', e.response.data.errors.email)
        })
    }



    render() {
        const { username, password } = this.state
        // const { user, addUser } = this.props
        // console.log(user)
        return (
            <ImageBackground source={require('./min.png')} style={styles.background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={[styles.layout1, styles.centerLayout1]}>
                            <Text style={styles.textStyle}>
                            </Text>
                            <View style={styles.center}>
                                <Image source={require('./movie2.png')} style={styles.profile} />
                                <Text style={styles.textLogo}>TICGO</Text>
                            </View>
                        </View>
                        <View style={[styles.layout2, styles.centerLayout2]}>
                            <InputItem
                                style={{ color: 'black' }}
                                clear
                                value={this.state.email}
                                onChange={email => {
                                    this.setState({
                                        email,
                                    });
                                }}
                                placeholder="E-mail"
                            >
                                <Image source={require('./email2.png')} style={styles.usernameIcon} />
                            </InputItem>
                            <InputItem
                                style={{ color: 'black' }}
                                clear
                                type="password"
                                secureTextEntry={true}
                                value={this.state.password}
                                onChange={password => {
                                    this.setState({
                                        password,
                                    });
                                }}
                                placeholder="Password"
                            >
                                <Image source={require('./key.png')} style={styles.passwordIcon} />
                            </InputItem>
                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    {/* <Button type="primary" onPress={() => this.onLogin()}>LOGIN</Button> */}
                                    <Button type="primary" onPress={() => this.goToMovie()}>LOGIN</Button>
                                </View>
                                <View style={styles.loginButton}>
                                    <Button  activeStyle={{ backgroundColor: 'white' }} onPress={() => this.goToRegister()}>REGISTER</Button>
                                </View>
                            </View>
                        </View>
                    </View>
                </View >

            </ImageBackground>

        );
    }
}

const styles = StyleSheet.create({

    background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 1,
        flexDirection: 'column'
    },

    layout2: {

        flex: 1,
        flexDirection: 'column'
    },

    loginmiddle: {
        flex: 0.5,
        
    },

    loginFooter: {
        flex: 0.5,
        flexDirection: 'row'
    },

    profile: {
        borderRadius: 10,
        width: 150,
        height: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },

    usernameIcon: {
        width: 25,
        height: 25
    },

    passwordIcon: {
        width: 30,
        height: 30
    },

    loginButton: {
        // backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 300,
       
    },
    textLogo: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15,
    },
    textStyle: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15
    },

    textButton: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        flex: 1,
        margin: 15
    },

})
const mapStateToProps = ({ user }) => ({
    username: user.username,
    password: user.password,
})

const mapDidpatchToProps = (dispatch) => {
    return {
        addUser: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDidpatchToProps
)(Login)

