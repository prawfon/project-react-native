import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, FlatList } from 'react-native';
import { SearchBar, TabBar, Card, WingBlank ,Icon} from '@ant-design/react-native';
import axios from 'axios'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'

class profile extends Component {

    onClickShowing = () => {
        this.props.history.push('/Movie')

    }
    onClickProfile = () => {
        this.props.history.push('/Profile')

    }
    onClickEdit = () => {
        this.props.history.push('/EditProfile')

    }

    onClickLogout = () => {
        this.props.history.push('/Login')

    }
    onClickBookingHistory = () => {
        this.props.history.push('/Booking')

    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Booking',
            items: [],
            isLoading: true,
            email: '',
            password: '',
            firstname: '',
            lastname: '',
            imagePath: '',
            historys: [],
            tickets: [],
            ticket: []

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: {
                Authorization: `Bearer ${user.user.token}`
            }
        }).then(response => {
            const data = response.data
            this.setState({ tickets: data, isLoading: false })
            for (let index = 0; index < data.length; index++) {
                //console.log('Ticket INdex:', data);
                axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${data[index].showtime}`)
                    .then(response => {
                        this.setState({ ticket: [...this.state.ticket, response.data] })
                        // console.log('ticket:', this.state.ticket);
                        // console.log('ticketS:', this.state.tickets);

                    })
            }
        })

            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    showDateTime = (item) => {
        var date = new Date(item.startDateTime).toDateString()
        var time = new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        return `${date} ${time}`
    }
    seatPrint = (seats) => {
        const seat = []
        for (let index = 0; index < seats.length; index++) {
          seat.push(<Text>{seats[index].type} Column: {seats[index].column} Row: {seats[index].row}</Text>)
        }
        return seat
      }
    


    render() {
        const { user } = this.props
        // console.log(user)
        // console.log(this.state.historys);

        return (


            <TabBar
            unselectedTintColor="white"
            tintColor="black"
            barTintColor="black"
        >
                <TabBar.Item
                    title="MOVIES"
                    icon={<Icon name="video-camera" />}
                    selected={this.state.selectedTab === 'Movie'}
                    onPress={() => this.onClickShowing()}
                />


                <TabBar.Item
                    icon={<Icon name="heart" />}
                    title="MY MOVIES"
                //selected={this.state.selectedTab === 'Faverite'}
                //onPress={this.onClickFaverite}
                />

                <TabBar.Item
                     icon={<Icon name="reload-time" />}
                     title="BOOKING"
                    selected={this.state.selectedTab === 'Booking'}
                    onPress={() => this.onClickBookingHistory()}
                >
                    <View style={[styles.content]}>
                        <View style={[styles.boxColumn]}>
                            <View style={[styles.boxHeader]}>
                                <View style={[styles.rowHeader]}>
                                    <Text style={[styles.textHead]}>Booking History</Text>
                                </View>
                            </View>
                            <View style={[styles.box1]}>
                                <ScrollView>
                                    <FlatList
                                        inverted data={this.state.ticket}
                                        renderItem={(item, index) => (
                                            <WingBlank size="lg">
                                                <TouchableOpacity>
                                                    <Card style={{ margin: 5 }}>
                                                        <Card.Header style={{ fontSize: 10 }}
                                                            title={item.item.movie.name}
                                                            thumbStyle={{ width: 30, height: 30 }}
                                                            extra={this.showDateTime(item.item)}
                                                        />
                                                        <Card.Body>
                                                            <View style={{ height: 42 }}>
                                                           
                                                            </View>
                                                        </Card.Body>

                                                    </Card>
                                                </TouchableOpacity>
                                            </WingBlank>
                                        )
                                        }
                                    />
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="PROFILE"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={() => this.onClickProfile()}
                />


            </TabBar>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(profile)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    img: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 200,
        height: 200,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373',
        margin: 10
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'white',
    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: 'black',
        borderBottomWidth: 3,
        borderBottomColor: 'black',
        justifyContent: 'center',
        padding: 15,


    },
    boxContent: {
        //flexDirection: 'row',
        backgroundColor: 'black',
        justifyContent: 'center',
        margin: 10

    },
    rowHeader: {
        flex: 1,
    },
    boxColumn: {
        flex: 1,
        backgroundColor: 'black',
        flexDirection: 'column'
    },
    box1: {
        flex: 1,
        backgroundColor: 'white'

    },
    box2: {
        flex: 1,
    },
    textStyle: {
        fontSize: 20
    }
});
