import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { InputItem, WhiteSpace, Button } from '@ant-design/react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker'
import axios from 'axios';


class EditProfile extends Component {
    state = {
        items: [],
        username: '',
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        imagePath: ''
    };

   
    goToProfile= () => {
        this.props.history.push('/Profile')
    }
    onSave = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${this.props.user.user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/ticGo/users', {
                headers: {
                    'Authorization': `Bearer ${this.props.user.user.token}`
                },
            }
            ).then(response => {
                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/Profile', {
                    item: 'profile'
                })
            }).catch(e => {
                console.log("error " + e)
            })
        })
            
    }

    
    selectImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${user.user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    firstName:response.data.user.firstName,
                    lastName:response.data.user.lastName,
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => { console.log('data',err) })
            .finally(() => { console.log('Finally') })
    }



    render() {
        const { user, editUser } = this.props
        console.log('praw: ',user)
        console.log(this.state.items);
        return (
            <ImageBackground source={require('./cinema.jpg')} style={styles.background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={styles.header}>
                            <View style={styles.headerBox1}>
                                <Text style={styles.text}>Edit profile</Text>
                            </View>
                            <View style={[styles.center, styles.headerBox2]}>
                                {/* <Image source={require('./popcorn-icon.png')} style={styles.popcorn} /> */}
                                <View>
                                    <TouchableOpacity onPress={this.goToProfile}>
                                        <Text style={{ fontSize: 13, color: 'black', fontWeight: 'bold' }}>Back</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>


                        <View style={[styles.layout2, styles.centerLayout2]}>
                            {/* <Text style={styles.text}>E-mail: {user[0].user.email}</Text>
                            <Text style={styles.text}>First name: {user[0].user.firstName}</Text>
                            <Text style={styles.text}>Last name: {user[0].user.lastName}</Text> */}
                            {/* <InputItem
                                type="email"
                                value={this.state.email}
                                // value={user[0].user.email}
                                onChange={email => { this.setState({ email, }); }}
                                placeholder="E-mail"
                            >
                            </InputItem> */}
                       
                            <InputItem
                                type="text"
                                clear
                                value={this.state.firstName}
                                // value= {user[0].user.firstName}
                                onChange={firstName=> { this.setState({ firstName , }); }}
                                placeholder="First Name"
                            >
                            </InputItem>
                            <InputItem
                                type="text"
                                clear
                                value={this.state.lastName}
                                // value={user[0].user.lastName}
                                onChange={lastName => { this.setState({ lastName, }); }}
                                placeholder="Last Name"
                            >
                            </InputItem>
                            
                            
                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    {/* <TouchableOpacity onPress={this.goToHome}>
                                <WhiteSpace />
                                    <Text style={styles.textButton}>Login</Text>
                                </TouchableOpacity> */}
                                
                                 <Button  onPress={() => this.onSave()}>SAVE</Button>
                                   
                                    <WhiteSpace />
                                </View>
                            </View>

                            {/* <View style={styles.loginFooter}> */}
                            {/* <View>
                                <TouchableOpacity onPress={this.goToRegister}>
                                    <Text style={{fontSize: 17, color: 'black'}}>Register</Text>
                                </TouchableOpacity>
                            </View> */}

                            {/* </View> */}

                        </View>
                    </View>
                </View >

            </ImageBackground>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            // console.log(firstName)
            // console.log(lastName)
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)


const styles = StyleSheet.create({

    background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flex: 0.15,
        flexDirection: 'row'
    },

    headerBox1: {
        flex: 1,
        backgroundColor: 'white'
    },

    headerBox2: {
        flex: 0.3,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 0.5,
        flexDirection: 'column'
    },

    layout2: {

        flex: 1,
        flexDirection: 'column'
    },
    text: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
        // padding: 30

    },
    loginmiddle: {
        flex: 0.5,
        flexDirection: 'row'
    },

    loginFooter: {
        flex: 0.5,
        flexDirection: 'row'
    },

    profile: {
        borderRadius: 10,
        width: 100,
        height: 100
    },

    usernameIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    passwordIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    loginButton: {
        backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 100,
        height: 40
    },

    textStyle: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15
    },

    textButton: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        flex: 1,
        margin: 15
    }

})
