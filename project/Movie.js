import React, { Component } from "react";
import { View, Text,Image, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import { Icon, TabBar, Grid } from '@ant-design/react-native';
import axios from "axios";
import History from './History'

// import { Button } from "@ant-design/react-native";
// import { log } from "core-js";

class movies extends Component {
    state = {
        items: []
    };
    // goToProfile = () => {
    //     this.props.history.push('/Profile', { user: this.state.email, password: this.state.password })
    // }
    goToLogin = () => {
        this.props.history.push('/Login')
    }
    goToProfile = () => {
        this.props.history.push('/Profile')
    }
    goToMovie = () => {
        this.props.history.push('/Movie')
    }
    onClickMovie = (item) => {
        this.props.history.push('/detailBooking', { item: item })

    }
    onClickHistory = (item) => {
        this.props.history.push('/History', { item: item })

    }

    componentDidMount() {
        this.getMovie()
    }
    getMovie = () => {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => {
                this.setState({
                    items: response.data,
                    isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    // onClickMovie = () => {
    //     this.props.history.push('/Register')

    // }

    render() {

        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="balck"
                barTintColor="black"
            >
                <TabBar.Item
                    title="MOVIES"
                    icon={<Icon name="video-camera" />}
                    selected={this.state.selectedTab === 'Movie'}
                    onPress={() => this.getMovie()}
                >
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.box1}>
                                {/* <Image source={require('./star.png')} style={{ width: 60, height: 60 }} /> */}
                                <Text style={styles.textHead}>MOVIES</Text>
                            </View>
                        </View>
                        <ScrollView>
                            {!this.state.isLoading ? (
                                <View style={styles.boxContent}>

                                    <Grid
                                        data={this.state.items}
                                        columnNum={2}
                                        renderItem={(item) => (
                                            <TouchableOpacity
                                                onPress={() => this.onClickMovie(item)}
                                            >
                                                <Image source={{ uri: item.image }}
                                                    style={{ width: '100%', height: 250 }} />
                                                <Text style={styles.textMovieName}>{item.name}</Text>
                                            </TouchableOpacity>
                                        )}
                                        itemStyle={{ height: 270, margin: 10 }}
                                    />
                                </View>
                            ) : (
                                    <ActivityIndicator size="large" color="#0000ff" style={[styles.center]} />
                                )}
                            <View style={{ padding: 30 }}>
                                <View style={[styles.rowHeader]}>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </TabBar.Item>
                {/* <TabBar.Item
                    icon={<Icon name="heart" />}
                    title="MY MOVIES"
                //   selected={this.state.selectedTab === 'Faverite'}
                //   onPress={this.onClickFaverite}
                /> */}

                <TabBar.Item
                    icon={<Icon name="reload-time" />}
                    title="HISTORY"
                  selected={this.state.selectedTab === 'History'}
                  onPress={() => this.onClickHistory('History')}
                >
                    <History />
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="PROFILE"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={this.goToProfile}
                />
            </TabBar>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },

    header: {
        backgroundColor: 'white',
        flex: 0.1,
        flexDirection: 'column',
        backgroundColor: 'black',
        borderBottomWidth: 3,
        justifyContent: 'center',
        padding: 20,

    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    boxContent: {
        //flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'center',
        margin: 10

    },

    row: {
        backgroundColor: 'black',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    },
    textHead: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    textMovieName: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',
    },
    rowHeader: {
        flex: 1,
    },
    footer: {
        backgroundColor: 'white',
        flex: 0.1,
        flexDirection: 'row',
        backgroundColor: '#1ED3F2',
        borderBottomWidth: 3,
        justifyContent: 'center',
        padding: 30,
    },
    box1: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black'
    },

    box: {
        flex: 1,
        margin: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1ED3F2'
    },

    logoSize: {
        borderRadius: 10,
        width: 30,
        height: 30
    },


})

export default movies
